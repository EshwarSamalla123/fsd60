package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping("SayHello")
	public String sayHello(){
		return "Hello from Controller!!!";
	}
	
	@RequestMapping("Welcomee")
	public String welcome(){
		return "Welcome from HelloController";
	}

	
}



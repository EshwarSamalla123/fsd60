@WebServlet("/product")
public class ProductServlet extends HttpServlet {
    private ProductDAO productDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        productDAO = new ProductDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "create":
                createProduct(request, response);
                break;
            case "update":
                updateProduct(request, response);
                break;
            case "delete":
                deleteProduct(request, response);
                break;
            default:
                response.sendRedirect("products.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "read":
                readProducts(request, response);
                break;
            default:
                response.sendRedirect("products.jsp");
        }
    }

    private void createProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve product details from request parameters
        String productName = request.getParameter("productName");
        // Other product details
        
        // Create a new product object
        Product product = new Product(productName, /* Other details */);
        
        // Call DAO method to insert the product into the database
        productDAO.createProduct(product);
        
        // Redirect to a suitable page upon successful creation
        response.sendRedirect("products.jsp");
    }

    private void readProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Call DAO method to retrieve list of products from the database
        List<Product> products = productDAO.getAllProducts();
        
        // Set products attribute to be accessed in JSP
        request.setAttribute("products", products);
        
        // Forward the request to the products JSP page for rendering
        request.getRequestDispatcher("products.jsp").forward(request, response);
    }

    private void updateProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve product details from request parameters
        int productId = Integer.parseInt(request.getParameter("productId"));
        String productName = request.getParameter("productName");
        // Other product details
        
        // Create a new product object with updated details
        Product product = new Product(productId, productName, /* Other details */);
        
        // Call DAO method to update the product in the database
        productDAO.updateProduct(product);
        
        // Redirect to a suitable page upon successful update
        response.sendRedirect("products.jsp");
    }

    private void deleteProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve product ID from request parameters
        int productId = Integer.parseInt(request.getParameter("productId"));
        
        // Call DAO method to delete the product from the database
        productDAO.deleteProduct(productId);
        
        // Redirect to a suitable page upon successful deletion
        response.sendRedirect("products.jsp");
    }
}

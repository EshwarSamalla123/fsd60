package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Register")
public class Register extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		String empName = request.getParameter("empName");
		double course = Double.parseDouble(request.getParameter("course"));
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		out.print("<h1>Employee Registration</h1>");
		out.print("<h3>EmpID   : " + empId + "</h3>");
		out.print("<h3>EmpName : " + empName + "</h3>");
		out.print("<h3>Salary  : " + course + "</h3>");
		out.print("<h3>Gender  : " + gender + "</h3>");
		out.print("<h3>Email-Id: " + emailId + "</h3>");
		out.print("<h3>Password: " + password + "</h3>");
		out.print("<center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

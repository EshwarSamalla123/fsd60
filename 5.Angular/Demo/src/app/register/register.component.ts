// // import { Component } from '@angular/core';
// // import { FormGroup } from '@angular/forms';
// // @Component({
// //   selector: 'app-register',
// //   templateUrl: './register.component.html',
// //   styleUrls: ['./register.component.css']
// // })
// // export class RegisterComponent {
// //   employee: any = {
// //     empName: '',
// //     salary: '',
// //     gender: '',
// //     doj: '',
// //     country: '',
// //     emailId: '',
// //     password: '',
// //     confirmPassword: '',
// //     mobileNumber: '',
// //     deptId: ''
// //   };

// //   constructor() { }

// //   registerEmployee() {
// //     // Retrieve registered employees from localStorage
   

// //     // Save the updated array back to localStorage

// //     // Clear the form fields after registration
// //     this.employee = {
// //       empName: '',
// //       salary: '',
// //       gender: '',
// //       doj: '',
// //       country: '',
// //       emailId: '',
// //       password: '',
// //       confirmPassword: '',
// //       mobileNumber: '',
// //       deptId: ''
// //     };

    
// //   }
// // }

// // import { Component } from '@angular/core';
// // import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// // @Component({
// //   selector: 'app-register',
// //   templateUrl: './register.component.html',
// //   styleUrls: ['./register.component.css']
// // })
// // export class RegisterComponent {
// //   registerForm: FormGroup;

// //   constructor(private formBuilder: FormBuilder) {
// //     this.registerForm = this.formBuilder.group({
// //       empName: ['', Validators.required],
// //       emailId: ['', [Validators.required, Validators.email]],
// //       password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
// //       confirmPassword: [''],
// //       mobileNumber: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
// //       deptId: ['']
// //     });
// //   }

// //   registerEmployee() {
// //     if (this.registerForm.valid) {
// //       // Registration logic here
// //       console.log(this.registerForm.value);

// //       // Clear the form fields after registration
// //       this.registerForm.reset();
// //     } else {
// //       // Mark all fields as touched to display validation errors
// //       Object.values(this.registerForm.controls).forEach(control => {
// //         control.markAsTouched();
// //       });
// //     }
// //   }
// // }

// import { Component } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';


// // Add the confirmPasswordValidator function
// export function confirmPasswordValidator(control: FormGroup): { [key: string]: boolean } | null {
//   const password = control.get('password');
//   const confirmPassword = control.get('confirmPassword');

//   if (password && confirmPassword && password.value !== confirmPassword.value) {
//     return { 'passwordMismatch': true };
//   }

//   return null;
// }

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent {
//   registerForm: FormGroup;

//   constructor(private formBuilder: FormBuilder,) {
//     this.registerForm = this.formBuilder.group({
//       empName: ['', Validators.required],
//       salary: [''],
//       gender: [''],
//       doj: [''],
//       country: [''],
//       emailId: ['', [Validators.required, Validators.email]],
//       password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
//       confirmPassword: ['', Validators.required],
//       mobileNumber: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
//       deptId: ['']
//     }, { validator: confirmPasswordValidator }); // Apply confirmPasswordValidator
//   }

//   registerEmployee() {
//     if (this.registerForm.valid) {
//       // Registration logic here
//       console.log(this.registerForm.value);

//       // Clear the form fields after registration
//       this.registerForm.reset();
//     } else {
//       // Mark all fields as touched to display validation errors
//       Object.values(this.registerForm.controls).forEach(control => {
//         control.markAsTouched();
//       });
//     }
//   }
// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  emp: any;
  countries: any;
  departments: any;

  constructor(private router: Router, private service: EmpService) {

    //Making the emp object in such a way that it contains the department json object
    this.emp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId:''
      }
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { 
      console.log(data);
      this.departments = data; 
    });
  }

  empRegister(regForm: any) {

    //Binding the registerForm data to the emp object, as it contains the department as a Json object
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;   
    
    console.log(this.emp);
    
    this.service.registerEmplooyee(this.emp).subscribe((data: any) => { console.log(data); });

    alert("Employee Registered Successfully!!!");
    this.router.navigate(['login']);
  }
}

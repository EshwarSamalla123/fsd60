import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { RegisterComponent } from './register/register.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { ProductComponent } from './product/product.component';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    ShowemployeesComponent,
    RegisterComponent,
    ExpPipe,
    GenderPipe,
    ShowempbyidComponent,
    ProductComponent,
    HeaderComponent,
    LogoutComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

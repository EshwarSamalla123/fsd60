package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Get All Records from Employee Table
public class Demo06 {
    public static void main(String[] args) {

        Connection connection = DbConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String selectQuery = "SELECT * FROM employee";

        try {
            preparedStatement = connection.prepareStatement(selectQuery);
            resultSet = preparedStatement.executeQuery();

            if (resultSet != null) {

                while (resultSet.next()) {
                    System.out.print(resultSet.getInt(1) + " " + resultSet.getString(2) + " ");
                    System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
                    System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
                    System.out.println("\n");
                }

            } else {
                System.out.println("No Record(s) Found!!!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}

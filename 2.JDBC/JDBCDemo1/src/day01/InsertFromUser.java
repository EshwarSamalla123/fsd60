package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertFromUser {
    public static void main(String[] args) {

        Connection connection = DbConnection.getConnection();
        Statement statement = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter employee ID:");
        int empId = scanner.nextInt();
        scanner.nextLine(); 
        
        System.out.println("Enter employee name:");
        String empName = scanner.nextLine();
        
        System.out.println("Enter employee salary:");
        double salary = scanner.nextDouble();
        scanner.nextLine(); 

        System.out.println("Enter employee gender:");
        String gender = scanner.nextLine();

        System.out.println("Enter employee email ID:");
        String emailId = scanner.nextLine();

        System.out.println("Enter employee password:");
        String password = scanner.nextLine();

        String insertQuery = "insert into employee values (" +
                empId + ", '" + empName + "', " + salary + ", '" +
                gender + "', '" + emailId + "', '" + password + "')";

        try {
            statement = connection.createStatement();
            int result = statement.executeUpdate(insertQuery);

            if (result > 0) {
                System.out.println(result + " Record(s) Inserted...");
            } else {
                System.out.println("Record Insertion Failed...");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (connection != null) {
                statement.close();
                connection.close();
            }
            scanner.close(); 
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

Auto_Increment
--------------
create table student1 (
sid int primary key auto_increment,
age int);

desc student1;

insert into student1 (age) values (22);
select * from student1;

insert into student1 (age) values (23);
insert into student1 (age) values (24);
select * from student1;

insert into student1 values (101, 12);
select * from student1;

insert into student1 (age) values (23);
insert into student1 (age) values (24);
select * from student1;


drop table student1;



Set the auto_increment value starting from 1001 at the time of table creation
-----------------------------------------------------------------------------
create table student1 (
sid int primary key auto_increment,
age int) auto_increment=1001;

insert into student1 (age) values (22);
insert into student1 (age) values (23);
select * from student1;






Referential Integrity
---------------------
1. On Delete Default
2. On Delete Cascade
3. On Delete Set Null



1. On Delete Default
====================
When you are deleting a record from the parent table and the parent tables corresponding record is associated with the child table record(s), you are not able to delete the parent's table record.

To do it, first you need change the department id of the corresponding records or you need to first delete the corresponding records from the child table, then you can delete the records from the parent table.



Create Department Table
-----------------------
create table department (
deptId int(4) primary key, 
deptName varchar(20),
location varchar(20));

insert into department values 
(10, 'IT', 'Hyd'),
(20, 'Sales', 'Chennai'),
(30, 'Testing', 'Pune');

Select * from department;



Create Employee Table Referencing to Department(deptId)
-------------------------------------------------------
create table employee (
empId int(4) primary key, 
empName varchar(20),
salary double(8, 2),
gender varchar(6),
emailId varchar(20) unique key,
password varchar(20),
deptId int,
foreign key (deptId) references department(deptId));

 
insert into employee values (101, 'Harsha', 1212.12, 'Male', 'harsha@gmail.com', '123', 40);
Failed as their is no deptId with 40

insert into employee values (101, 'Harsha',  1212.12, 'Male',   'harsha@gmail.com',  '123', 10);
insert into employee values (102, 'Pasha',   1212.12, 'Male',   'pasha@gmail.com',   '123', 10);
insert into employee values (103, 'Indira',  1212.12, 'Female', 'indira@gmail.com',  '123', 20);
insert into employee values (104, 'Vamsi',   1212.12, 'Male', '  vamsi@gmail.com',   '123', 20);
insert into employee values (105, 'Deepika', 1212.12, 'Female', 'deepika@gmail.com', '123', 30);
insert into employee values (106, 'Utkarsh', 1212.12, 'Male',   'utkarsh@gmail.com', '123', 30);

Select * from employee;



select * from department;
select * from employee;



delete from department where deptId=30;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails (`fsd60`.`employee`, CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`deptId`) REFERENCES `department` (`deptId`))

delete from employee where empId = 105;
delete from employee where empId = 106;
select * from employee;

delete from department where deptId=30;
select * from department;


drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.

drop table employee;
drop table department;












2. On Delete Cascade
====================
When you are deleting a record from the parent table and the parent tables corresponding record is associated with the child table record(s) are also getting deleted.


Create Department Table
-----------------------
create table department (
deptId int(4) primary key, 
deptName varchar(20),
location varchar(20));

insert into department values 
(10, 'IT', 'Hyd'),
(20, 'Sales', 'Chennai'),
(30, 'Testing', 'Pune');

Select * from department;



Create Employee Table Referencing to Department(deptId)
-------------------------------------------------------
create table employee (
empId int(4) primary key, 
empName varchar(20),
salary double(8, 2),
gender varchar(6),
emailId varchar(20) unique key,
password varchar(20),
deptId int,
foreign key (deptId) references department(deptId) on delete cascade);

 
insert into employee values (101, 'Harsha',  1212.12, 'Male',   'harsha@gmail.com',  '123', 10);
insert into employee values (102, 'Pasha',   1212.12, 'Male',   'pasha@gmail.com',   '123', 10);
insert into employee values (103, 'Indira',  1212.12, 'Female', 'indira@gmail.com',  '123', 20);
insert into employee values (104, 'Vamsi',   1212.12, 'Male',   'vamsi@gmail.com',   '123', 20);
insert into employee values (105, 'Deepika', 1212.12, 'Female', 'deepika@gmail.com', '123', 30);
insert into employee values (106, 'Utkarsh', 1212.12, 'Male',   'utkarsh@gmail.com', '123', 30);

Select * from employee;



select * from department;
select * from employee;


delete from department where deptId=30;
select * from department;
select * from employee;

drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.

drop table employee;
drop table department;







3. On Delete Set Null
=====================
When you are deleting a record from the parent table and the parent tables corresponding record is associated with the child table record(s) are initialized with null.


Create Department Table
-----------------------
create table department (
deptId int(4) primary key, 
deptName varchar(20),
location varchar(20));

insert into department values 
(10, 'IT', 'Hyd'),
(20, 'Sales', 'Chennai'),
(30, 'Testing', 'Pune');

Select * from department;



Create Employee Table Referencing to Department(deptId)
-------------------------------------------------------
create table employee (
empId int(4) primary key, 
empName varchar(20),
salary double(8, 2),
gender varchar(6),
emailId varchar(20) unique key,
password varchar(20),
deptId int,
foreign key (deptId) references department(deptId) on delete set null);

 
insert into employee values (101, 'Harsha',  1212.12, 'Male',   'harsha@gmail.com',  '123', 10);
insert into employee values (102, 'Pasha',   1212.12, 'Male',   'pasha@gmail.com',   '123', 10);
insert into employee values (103, 'Indira',  1212.12, 'Female', 'indira@gmail.com',  '123', 20);
insert into employee values (104, 'Vamsi',   1212.12, 'Male',   'vamsi@gmail.com',   '123', 20);
insert into employee values (105, 'Deepika', 1212.12, 'Female', 'deepika@gmail.com', '123', 30);
insert into employee values (106, 'Utkarsh', 1212.12, 'Male',   'utkarsh@gmail.com', '123', 30);

Select * from employee;


select * from department;
select * from employee;


delete from department where deptId=30;
select * from department;
select * from employee;

drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.

drop table employee;
drop table department;


--Done with Referential Integrity
